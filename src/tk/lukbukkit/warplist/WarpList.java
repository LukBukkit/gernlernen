package tk.lukbukkit.warplist;

import java.io.File;
import java.util.ArrayList;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

public class WarpList extends JavaPlugin {

    public static ArrayList<String> warpnames = new ArrayList<>();
    private static WarpList plugin;

    @Override
    public void onDisable() {

    }

    @Override
    public void onEnable() {
        plugin = this;
        File[] files = new File("plugins" + File.separator + "Essentials" + File.separator + "warps" + File.separator).listFiles();

        for (File file : files) {
            if (file.isFile() && file.getName().endsWith(".yml")) {
                FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);
                String name = fileConfig.getString("name");
                warpnames.add(name);
                System.out.println("Deteced Warp : " + name);
            }
        }

        generateConfig();

        this.getCommand("warper").setExecutor(new tk.lukbukkit.warplist.commands.Warper());

        PluginManager pm = this.getServer().getPluginManager();
        pm.registerEvents(new tk.lukbukkit.warplist.events.CommandPreProgress(), this);
    }

    public static String getLangTrans(String name) {
        String st = plugin.getConfig().getString(name);
        return ChatColor.translateAlternateColorCodes('&', st);
    }

    private void generateConfig() {
        this.getConfig().addDefault("lang.warp.added.1", "The warp");
        this.getConfig().addDefault("lang.warp.added.2", "was added to the list!");

        this.getConfig().addDefault("lang.warp.removed.1", "The warp");
        this.getConfig().addDefault("lang.warp.removed.2", "was removed from the list!");

        this.getConfig().addDefault("lang.thewarps", "&4The warps:");

        this.getConfig().addDefault("lang.teleportsyou", "Teleports you!");

        this.getConfig().addDefault("color.warpnames", "2");

        this.getConfig().options().copyDefaults(true);
        this.saveConfig();
    }

    public void resetConfig() {
        File configFile = new File(plugin.getDataFolder(), "config.yml");
        configFile.delete();
        saveDefaultConfig();
        reloadConfig();
    }

    public static WarpList getInstance() {
        return plugin;
    }
}
