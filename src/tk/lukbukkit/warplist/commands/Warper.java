package tk.lukbukkit.warplist.commands;

import java.util.Iterator;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.lukbukkit.warplist.WarpList;

public class Warper implements CommandExecutor {

    private String ps;

    private int in;

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (!(cs instanceof Player)) {
            cs.sendMessage("Du bist kein Spieler!");
        }
        Player p = (Player) cs;
        if (args.length > 0) {
            p.performCommand("essentials:warp " + args[0]);
            return true;
        } else {
            in = 1;
            p.sendMessage(ChatColor.RED + WarpList.getLangTrans("lang.thewarps"));
            TextComponent message = new TextComponent("");
            for (Iterator<String> it = WarpList.warpnames.iterator(); it.hasNext();) {
                String s = it.next();
                TextComponent extra = new TextComponent(s);
                extra.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/warp " + s));
                extra.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder(WarpList.getLangTrans("lang.teleportsyou")).create()));
                extra.setColor(net.md_5.bungee.api.ChatColor.getByChar(WarpList.getLangTrans("color.warpnames").charAt(0)));
                message.addExtra(extra);
                TextComponent komma = new TextComponent(ChatColor.GRAY + " | ");
                if (!(in == WarpList.warpnames.size())) {
                    message.addExtra(komma);
                }
                in++;
            }
            p.spigot().sendMessage(message);
            in = 1;

            return true;
        }
    }

}
