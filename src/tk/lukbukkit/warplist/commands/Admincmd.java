package tk.lukbukkit.warplist.commands;

import java.util.ArrayList;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import tk.lukbukkit.warplist.WarpList;

public class Admincmd implements CommandExecutor {

    private ArrayList<CommandSender> resetConfigCommandSender = new ArrayList<>();
    public static ArrayList<Player> resetConfig = new ArrayList<>();

    @Override
    public boolean onCommand(CommandSender cs, Command cmd, String label, String[] args) {
        if (!(cs instanceof Player)) {
            return false;
        }
        
        if (args[0].equalsIgnoreCase("resetconfig")) {
            final Player p = (Player) cs;
            p.sendMessage(ChatColor.RED + "Reset Config? Enter Yes / No");
            resetConfig.add(p);
            Bukkit.getScheduler().scheduleSyncDelayedTask(WarpList.getInstance(), new Runnable(){

                @Override
                public void run() {
                    resetConfig.remove(p);
                    p.sendMessage("The time is over!");
                }
            }, 20L*10);
            return true;
        }
        Player p = (Player) cs;
        
        return true;
    }

}
