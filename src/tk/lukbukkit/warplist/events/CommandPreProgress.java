package tk.lukbukkit.warplist.events;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import tk.lukbukkit.warplist.WarpList;

public class CommandPreProgress implements Listener {

    @EventHandler
    public void onCommnadPre(PlayerCommandPreprocessEvent e) {
        String[] message = e.getMessage().split(" ");
        String command = message[0];
        if (command.equalsIgnoreCase("/setwarp") || command.equalsIgnoreCase("/createwarp")) {
            if (e.getPlayer().hasPermission("essentials.setwarp") && message.length > 1) {
                String warpname = message[1].toLowerCase();
                if (!this.isInt(warpname)) {
                    WarpList.warpnames.add(warpname);
                    e.getPlayer().sendMessage(WarpList.getLangTrans("lang.warp.added.1") + " " + warpname + " " + WarpList.getLangTrans("lang.warp.added.2"));
                }
            }
        }
        if (command.equalsIgnoreCase("/delwarp") || command.equalsIgnoreCase("/remwarp") || command.equalsIgnoreCase("/rmwarp")) {
            if (e.getPlayer().hasPermission("essentials.delwarp") && message.length > 1) {
                String warpname = message[1].toLowerCase();
                if (WarpList.warpnames.contains(warpname)) {
                    e.getPlayer().sendMessage(WarpList.getLangTrans("lang.warp.removed.1") + " " + warpname + " " + WarpList.getLangTrans("lang.warp.removed.2"));
                    WarpList.warpnames.remove(warpname);
                }

            }
        }

    }

    //Taken from Essentials
    public boolean isInt(String sInt) {
        try {
            Integer.parseInt(sInt);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }

}
